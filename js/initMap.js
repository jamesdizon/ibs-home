function initMap() {
  // Styles a map in night mode.
  var ibs = {lat: -28.0024000, lng: 153.341899};
  var map = new google.maps.Map(document.getElementById('map'), {
    center: ibs,
    zoom: 15,
    disableDefaultUI: true,
    styles: [
      {elementType: 'geometry', stylers: [{color: '#333333'}]},
      {elementType: 'labels.text.stroke', stylers: [{color: '#2B2B2B'}]},
      {elementType: 'labels.text.fill', stylers: [{color: '#8C8C8C'}]},
      {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{color: '#8C8C8C'}]
      },
      {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{color: '#464646'}]
      },
      {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{color: '#222222'}]
      },
      {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{color: '#464646'}]
      },
      {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{color: '#000000'}]
      },
      {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{color: '#000000'}]
      },
      {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{color: '#8C8C8C'}]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{color: '#000000'}]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{color: '#000000'}]
      },
      {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{color: '#8C8C8C'}]
      },
      {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{color: '#111111'}]
      },
      {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{color: '#8C8C8C'}]
      },
      {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{color: '#2B2B2B'}]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{color: '#8C8C8C'}]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{color: '#00090f'}]
      }
    ]
  });
  var marker = new google.maps.Marker({
    position: ibs,
    map: map
  });
}
